//@ts-check
const server = require('net');
const port = 31457;
const host = '127.0.0.1';

const decodeLogin = require('./protocol/decodeLogin');
const tetrinetProtocolIO = require('./protocol/IO');


server.createServer((socket) => {
  
  socket.on('data', (data) => {
    const str = tetrinetProtocolIO.read(data);
    var decodedString = decodeLogin( str );

    // remove unreadeble char from decoded string
    //decodedString = decodedString.slice(0, decodedString.length - 1);
    console.log(decodedString);
    var clientVersion = decodedString.split(' ')[2];
    var playerName = decodedString.split(' ')[1];
    var startMessage = decodedString.split(' ')[0];

    console.log("cliente versao: " + clientVersion)
    console.log("comando init:" + startMessage);
    console.log("nome jogador:" +  playerName);

    if(startMessage === 'tetrisstart' && clientVersion === '1.13'){
      // login approved
      console.log("login aprovado");

      let winlist = tetrinetProtocolIO.encode("winlist tTourTheAbyss;922 pFregge;855 pelissa;475");
      socket.write(winlist);

      let loginResponse = tetrinetProtocolIO.encode("playernum 1");
      socket.write(loginResponse);
     
    }
 
  });

  socket.on('error', (err) => {
    console.log("error client" + err);
  });

  socket.on('end', () => {
    console.log("end connection");
  });
})
.listen(port, host, () => console.log(`Server listening on ${host}:${port}`));
